#!/bin/bash

docker kill $(docker ps -q)
docker rm $(docker ps -a -q)
docker rmi $(docker images -q)

docker system prune --all -f
docker volume prune -f

docker-compose -f docker-compose-gitlab.yml up -d && docker-compose -f docker-compose-gitlab.yml logs -f
