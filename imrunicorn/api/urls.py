from django.urls import path, include
from . import views
from content_collection import views as c_views
# from meme_leach import views as meme_leach_views
from rest_framework import routers
from django.views.generic import TemplateView
# from api.views import ChartData as GroundHogChartData
from groundhog_logbook.views import ChartDataBySex
# from api.views import ChartDataTest
from .views import ActivityLogViewSet

# similar to object based url building
router = routers.DefaultRouter()
router.get_api_root_view().cls.__name__ = "Data Simplified"
router.get_api_root_view().cls.__doc__ = "Data is the backbone to a digital wonderland."

router.register('accounts', views.Accounts)

router.register('activity_log/activity', views.ActivityViewSet)
router.register('activity_log/activity_log', views.ActivityLogViewSet)
router.register('activity_log/activity_photo_validation', views.ActivityPhotoValidationViewSet)

router.register('announcements/what_is_new', views.WhatIsNewView)
router.register('announcements/main_page_blurbs', views.MainPageBlurbsView)
router.register('announcements/page_blurb_overrides', views.PageBlurbOverridesView)

router.register('content_collection/insults', views.ContentCollectionInsultsViewSet)
router.register('content_collection/SensorReading', views.SensorReadingViewSet, basename='SensorReading')

router.register('content_collection/LastSensorReading', views.LastSensorReadingViewSet)

router.register('deer_harvest_logbook/Harvests', views.HarvestsView)
router.register('deer_harvest_logbook/HarvestsPhotos', views.HarvestsPhotosView)

router.register('deer_wait_list/Recipient', views.DeerWaitListRecipient)
router.register('deer_wait_list/MeatCut', views.DeerWaitListMeatCut)
router.register('deer_wait_list/Flavor', views.DeerWaitListFlavor)
router.register('deer_wait_list/RequestedOrder', views.DeerWaitListRequestedOrder)

router.register('docker_hub_webhook', views.DockerWebhookView)

router.register('groundhog_logbook/Location', views.LocationView)
router.register('groundhog_logbook/RemovalsByLocation', views.RemovalsByLocationView)

# router.register('loaddata/Owner', views.Owner)        # this causes a bug with /api/accounts
router.register('loaddata/Caliber', views.LoadDataCaliber)
router.register('loaddata/Firearm', views.LoadDataFirearm)
router.register('loaddata/Powder', views.LoadDataPowder)
router.register('loaddata/Projectile', views.LoadDataProjectile)
router.register('loaddata/Brass', views.LoadDataBrass)
router.register('loaddata/Primer', views.LoadDataPrimer)
router.register('loaddata/HandLoad', views.LoadDataHandLoad)
router.register('loaddata/EstimatedDope', views.LoadDataEstimatedDope)

router.register('meme_leach', views.MemeLeachViewSet)
router.register('motion_readings', views.MotionReadingsViewSet)

router.register('shooting_challenge/ChallengeEvent', views.ChallengeEventViewSet)
router.register('shooting_challenge/ChallengePhoto', views.ChallengePhotoViewSet)


# HyperlinkedModelSerializer doesn't like namespace addressing for the 'url' to work
# app_name = 'api'
urlpatterns = [
    # path('DeerWaitListRecipient/', views.DeerWaitListRecipient, name='DeerWaitListRecipient'),
    # path("activity_log", ActivityLogViewSet.as_view(), name='activity_log'),
    path('', include(router.urls)),
    # this is used for docker hub to push via webhook. It's not classy, but it works.
    path('docker_hub_hook/', views.docker_hub_webhook_unclassy, name='docker_hub_webhook_unclassy'),
    path('sensor_reading/', c_views.sensor_readings, name='sensor_readings'),
    path('meme_leach/<str:subreddit>', views.random_meme_by_subreddit),
]
