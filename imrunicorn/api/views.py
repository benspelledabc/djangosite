from django.http import JsonResponse
from rest_framework.permissions import (
    IsAuthenticated,
    IsAdminUser,
    DjangoModelPermissions,
    DjangoObjectPermissions,
    DjangoModelPermissionsOrAnonReadOnly,
    AllowAny,
    IsAuthenticatedOrReadOnly,
)
from rest_framework.response import Response
from rest_framework.views import APIView
from glom import glom
from django.core import serializers
from django.http import HttpResponse

from rest_framework.response import Response
from datetime import datetime, date
from django.conf import settings
from django.core.mail import send_mail
from django.db.models import Q
from django.shortcuts import render
from rest_framework import viewsets, generics, status
from rest_framework.decorators import action
from django.shortcuts import redirect
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth.decorators import permission_required

# from rest_framework.permissions import IsAuthenticated
from django.contrib.auth.models import AnonymousUser, User

from .serializer import DockerHubWebhookSerializer
from .models import DockerHubWebhook

from imrunicorn.serializer import UserSerializer, UserProfileSerializer

from meme_leach.models import LeachedMeme, LeachedMemePreview
from meme_leach.serializer import LeachedMemeSerializer, LeachedMemePreviewSerializer
from meme_leach.functions import leach_post, leach_post_subreddit

from announcements.models import WhatIsNew, MainPageBlurbs, PageBlurbOverrides
from announcements.serializer import (
    WhatIsNewSerializer,
    MainPageBlurbsSerializer,
    PageBlurbOverridesSerializer,
)

from deer_wait_list.models import Recipient, MeatCut, Flavor, RequestedOrder
from deer_wait_list.serializer import (
    RecipientSerializer,
    MeatCutSerializer,
    FlavorSerializer,
    RequestedOrderSerializer,
)

from loaddata.models import (
    Caliber,
    Firearm,
    Powder,
    Projectile,
    Brass,
    Primer,
    HandLoad,
    EstimatedDope,
)
from loaddata.serializer import (
    CaliberSerializer,
    FirearmSerializer,
    OwnerSerializer,
    PowderSerializer,
    ProjectileSerializer,
    BrassSerializer,
    PrimerSerializer,
    HandLoadSerializer,
    EstimatedDopeSerializer,
)

from groundhog_logbook.models import Location, RemovalsByLocation
from groundhog_logbook.serializer import (
    LocationSerializer,
    RemovalsByLocationSerializer,
)

from deer_harvest_logbook.models import Harvests, HarvestPhoto
from deer_harvest_logbook.serializer import HarvestsSerializer, HarvestPhotoSerializer

from activity_log.models import ActivityPhotoValidation, ActivityLog, Activity
from activity_log.serializer import (
    ActivitySerializer,
    ActivityLogSerializer,
    ActivityPhotoValidationSerializer,
)

from shooting_challenge.models import ChallengePhoto, ChallengeEvent
from shooting_challenge.serializer import (
    ChallengeEventSerializer,
    ChallengePhotoSerializer,
)

from content_collection.models import RandomInsult, MotionReadings, SensorReadings
from content_collection.serializer import (
    RandomInsultSerializer,
    MotionReadingsSerializer,
    SensorReadingsSerializer,
)

from imrunicorn.functions import step_hit_count_by_page, get_weather

from groundhog_logbook.functions import (
    all_groundhog_removals,
    all_groundhog_removals_by_shooter,
    all_groundhog_hole_locations,
    groundhog_removal_scoreboard,
    groundhogs_by_hour_of_day,
    groundhogs_by_hour_of_day_by_sex,
    groundhogs_by_sex,
    groundhogs_count_by_sex,
    groundhog_removal_scoreboard_annual,
)


class Owner(viewsets.ModelViewSet):
    # require user to be logged on.
    permission_classes = (IsAuthenticated,)
    # fetch data
    queryset = User.objects.all()
    serializer_class = OwnerSerializer


# ############### motion_sensor ###############
class LastSensorReadingViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = SensorReadings.objects.all().order_by("-pk")[:1]
    serializer_class = SensorReadingsSerializer


class SensorReadingViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = SensorReadings.objects.all().order_by("-pk")
    serializer_class = SensorReadingsSerializer

    @action(detail=False)
    def H_Stickers(self, request):
        queryset = SensorReadings.objects.filter(Q(sensor_location="H_Stickers"))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def A_BR_Closet(self, request):
        queryset = SensorReadings.objects.filter(Q(sensor_location="A_BR_Closet"))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def A_LivingRoom(self, request):
        queryset = SensorReadings.objects.filter(Q(sensor_location="A_LivingRoom"))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def A_Winchester_1(self, request):
        queryset = SensorReadings.objects.filter(Q(sensor_location="A_Winchester_1"))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def UnDefined(self, request):
        queryset = SensorReadings.objects.filter(
            ~Q(sensor_location="H_Stickers"),
            ~Q(sensor_location="A_BR_Closet"),
            ~Q(sensor_location="A_Winchester_1"),
            ~Q(sensor_location="A_LivingRoom"),
        )
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# ############### content_collection ###############
class ContentCollectionInsultsViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = RandomInsult.objects.all()
    serializer_class = RandomInsultSerializer

    @action(detail=False)
    def random_insult(self, request):
        queryset = RandomInsult.objects.order_by("?")[:1]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# ############### motion_sensor ###############
class MotionReadingsViewSet(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticatedOrReadOnly,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = MotionReadings.objects.all().order_by("-pk")
    serializer_class = MotionReadingsSerializer

    @action(detail=False)
    def No_Message(self, request):
        queryset = MotionReadings.objects.filter(Q(message=""))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# ############### meme_leach ###############
class MemeLeachViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = LeachedMeme.objects.all().order_by("-pk")
    serializer_class = LeachedMemeSerializer

    @action(detail=False)
    def random_meme(self, request):
        unused_result = leach_post()
        queryset = LeachedMeme.objects.order_by("?")[:1]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def meme_not_safe_for_work(self, request):
        # just a general leach
        unused_result = leach_post()
        queryset = LeachedMeme.objects.filter(Q(nsfw=True)).order_by("-pk")

        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def meme_safe_for_work(self, request):
        # just a general leach
        unused_result = leach_post()
        queryset = LeachedMeme.objects.filter(Q(nsfw=False)).order_by("-pk")

        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def random_meme_by_subreddit(self, request):
        subreddit = request.query_params.get("id")
        if subreddit is None:
            subreddit = "none"

        unused_result = leach_post_subreddit(subreddit)
        queryset = LeachedMeme.objects.filter(Q(subreddit=subreddit.lower())).order_by(
            "?"
        )[:1]

        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


@api_view(["GET"])
def random_meme_by_subreddit(request, subreddit):
    unused_result = leach_post_subreddit(subreddit)
    if subreddit == "popular":
        # this is always the last one just added
        # queryset = LeachedMeme.objects.all().order_by('-pk')[:1]
        queryset = LeachedMeme.objects.all().order_by("?")[:1]
    else:
        queryset = LeachedMeme.objects.filter(Q(subreddit=subreddit.lower())).order_by(
            "?"
        )[:1]
    qs_json = serializers.serialize("json", queryset)
    return HttpResponse(qs_json, content_type="application/json")


# ############### shooting_challenge ###############
class ChallengePhotoViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = ChallengePhoto.objects.all()
    serializer_class = ChallengePhotoSerializer


class ChallengeEventViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    queryset = ChallengeEvent.objects.all()
    serializer_class = ChallengeEventSerializer

    @action(detail=False)
    def challenge_has_event_photos(self, request):
        queryset = ChallengeEvent.objects.filter(
            Q(challenge_photos__gt=0)
        )  # i dont think this is the right filter
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# ############### activity_log ###############
class ActivityPhotoValidationViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = ActivityPhotoValidation.objects.all()
    serializer_class = ActivityPhotoValidationSerializer


class ActivityLogViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = ActivityLog.objects.all()
    serializer_class = ActivityLogSerializer

    @action(detail=False)
    def approval_pending(self, request):
        queryset = ActivityLog.objects.filter(Q(approved=False))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def approval_complete(self, request):
        queryset = ActivityLog.objects.filter(Q(approved=True))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class ActivityViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAdminUser,)
    queryset = Activity.objects.all()
    serializer_class = ActivitySerializer

    @action(detail=False)
    def sfw_all(self, request):
        queryset = Activity.objects.filter(Q(sfw=True))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def tasks_all(self, request):
        queryset = Activity.objects.filter(Q(transaction_amount__gt=0))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def rewards_all(self, request):
        queryset = Activity.objects.filter(Q(transaction_amount__lte=0))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def random_task_all(self, request):
        queryset = Activity.objects.filter(Q(transaction_amount__gt=0)).order_by("?")[
            :1
        ]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def random_task_sfw(self, request):
        queryset = Activity.objects.filter(
            Q(transaction_amount__gt=0, sfw=True)
        ).order_by("?")[:1]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# ############### loaddata ###############
class LoadDataCaliber(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Caliber.objects.all()
    serializer_class = CaliberSerializer


# this does not work
# @permission_required('loaddata.view_firearm', login_url='/login', raise_exception=True)
class LoadDataFirearm(viewsets.ModelViewSet):
    # require user to be logged on.
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Firearm.objects.all()
    serializer_class = FirearmSerializer


class LoadDataPowder(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Powder.objects.all()
    serializer_class = PowderSerializer


class LoadDataProjectile(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Projectile.objects.all()
    serializer_class = ProjectileSerializer


class LoadDataBrass(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Brass.objects.all()
    serializer_class = BrassSerializer


class LoadDataPrimer(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Primer.objects.all()
    serializer_class = PrimerSerializer


class Accounts(viewsets.ModelViewSet):
    # permission_classes = (IsAuthenticated,)
    permission_classes = (IsAdminUser,)
    queryset = User.objects.all()
    serializer_class = UserSerializer

    @action(detail=False)
    def recent_users(self, request):
        recent_users = User.objects.filter(Q(last_login__isnull=False)).order_by(
            "-last_login"
        )

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def recent_users_reverse(self, request):
        recent_users = User.objects.filter(Q(last_login__isnull=False)).order_by(
            "last_login"
        )

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def never_logged_in(self, request):
        recent_users = User.objects.filter(Q(last_login__isnull=True))

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def active(self, request):
        recent_users = User.objects.filter(Q(is_active=True))

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def inactive(self, request):
        recent_users = User.objects.filter(Q(is_active=False))

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def staff(self, request):
        recent_users = User.objects.filter(Q(is_staff=True))

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def superusers(self, request):
        recent_users = User.objects.filter(Q(is_superuser=True))

        page = self.paginate_queryset(recent_users)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(recent_users, many=True)
        return Response(serializer.data)


class LoadDataHandLoad(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (IsAuthenticatedOrReadOnly,)

    # fetch data
    queryset = HandLoad.objects.all().order_by("-id")
    serializer_class = HandLoadSerializer

    @action(detail=False)
    def by_powder_id(self, request, powder_id=1):
        # Q(transaction_amount__gt=0)).order_by('?')[:1]
        # queryset = HandLoad.objects.all().order_by('powder')
        powder_id = self.request.query_params.get("powder_id", None)

        if powder_id is not None:
            powder_id = int(powder_id)
            queryset = HandLoad.objects.filter(Q(powder=powder_id))
        else:
            queryset = HandLoad.objects.all().order_by("powder")

        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class LoadDataEstimatedDope(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    # fetch data
    queryset = EstimatedDope.objects.all()
    serializer_class = EstimatedDopeSerializer


# ############### Deer Wait List ###############
class DeerWaitListRecipient(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Recipient.objects.all()
    serializer_class = RecipientSerializer


class DeerWaitListMeatCut(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = MeatCut.objects.all()
    serializer_class = MeatCutSerializer


class DeerWaitListFlavor(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = Flavor.objects.all()
    serializer_class = FlavorSerializer


class DeerWaitListRequestedOrder(viewsets.ModelViewSet):
    # require user to be logged on.
    # permission_classes = (IsAuthenticated,)
    permission_classes = (DjangoModelPermissions,)
    # fetch data
    queryset = RequestedOrder.objects.all()
    # serializer_class = FlavorSerializer
    serializer_class = RequestedOrderSerializer


# ############### announcements ###############
class WhatIsNewView(viewsets.ModelViewSet):
    # permission_classes = (IsAdminUser,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    # permission_classes = (DjangoObjectPermissions, )

    queryset = WhatIsNew.objects.all()
    serializer_class = WhatIsNewSerializer

    @action(detail=False)
    def random_one(self, request):
        queryset = WhatIsNew.objects.filter(Q(Published=True)).order_by("?")[:1]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

        # this doesnt filter out the non published ones!

    """ def random_one(self, request):
        queryset = WhatIsNew.objects.order_by("?")[:1]
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data) """

    @action(detail=False)
    def sticky(self, request):
        queryset = WhatIsNew.objects.filter(Q(Is_Sticky=True))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def published(self, request):
        queryset = WhatIsNew.objects.filter(Q(Published=True))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    # todo: This should require admin or something.. thats the point of non published articles
    @action(detail=False)
    def not_published(self, request):
        queryset = WhatIsNew.objects.filter(Q(Published=False))
        result = self.paginate_queryset(queryset)
        if result is not None:
            serializer = self.get_serializer(result, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


class MainPageBlurbsView(viewsets.ModelViewSet):
    queryset = MainPageBlurbs.objects.all()
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = MainPageBlurbsSerializer


class PageBlurbOverridesView(viewsets.ModelViewSet):
    queryset = PageBlurbOverrides.objects.all()
    permission_classes = (IsAuthenticatedOrReadOnly,)
    # permission_classes = (DjangoModelPermissions,)
    serializer_class = PageBlurbOverridesSerializer


# ############### groundhog_logbook ###############
class LocationView(viewsets.ModelViewSet):
    queryset = Location.objects.all()
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = LocationSerializer


class RemovalsByLocationView(viewsets.ModelViewSet):
    queryset = RemovalsByLocation.objects.all()
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = RemovalsByLocationSerializer


# ############### deer harvest logbook ############
class HarvestsView(viewsets.ModelViewSet):
    queryset = Harvests.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = HarvestsSerializer


class HarvestsPhotosView(viewsets.ModelViewSet):
    queryset = HarvestPhoto.objects.all()
    permission_classes = (DjangoModelPermissions,)
    serializer_class = HarvestPhotoSerializer


# ############### Docker Hook ############
class DockerWebhookView(viewsets.ModelViewSet):
    # permission_classes = (DjangoModelPermissions,)
    permission_classes = (DjangoModelPermissionsOrAnonReadOnly,)
    queryset = DockerHubWebhook.objects.all()
    serializer_class = DockerHubWebhookSerializer

    def list(self, request, *args, **kwargs):
        queryset = DockerHubWebhook.objects.all()
        page = self.paginate_queryset(queryset)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)


# i'll this is because of how the post comes from docker. we have to unbind it a bit.
# we might not have to if we had a better/different serializer, but this works for now.
# i did change it form 'change' to 'add' permissions though
@api_view(["POST"])
def docker_hub_webhook_unclassy(request):
    step_hit_count_by_page(request.path)
    perm_check = request.user.has_perm("api.add_dockerhubwebhook")
    if perm_check:
        if request.method == "POST":
            try:
                data_of_value = {
                    "push_data": request.data.get("push_data"),
                    "repository": request.data.get("repository"),
                    "repo_name": glom(request.data, "repository.repo_name"),
                    "tag": glom(request.data, "push_data.tag"),
                    "pusher": glom(request.data, "push_data.pusher"),
                }

                serializer = DockerHubWebhookSerializer(data=data_of_value)
                if serializer.is_valid():
                    serializer.save()
                    return Response(serializer.data, status=status.HTTP_201_CREATED)
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            except Exception as ex:
                return JsonResponse({"status": "error", "exception": ex})
    else:
        return JsonResponse(
            {"access": "denied", "permission_required": "api.add_dockerhubwebhook"}
        )
