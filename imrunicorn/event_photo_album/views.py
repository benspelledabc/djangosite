from django.shortcuts import render
from announcements.get_news import get_version_json, get_page_blurb_override
from imrunicorn.functions import step_hit_count_by_page
from content_collection.functions import leach_buzzword
from datetime import datetime


def page_home(request):
    step_hit_count_by_page(request.path)
    context = {
        "copy_year": datetime.now().year,
        'release': get_version_json(),
        "title": "Event Photo Album: Home",
        "blurb": get_page_blurb_override('event_photo_album/home/'),
    }
    return render(request, "event_photo_album/home.html", context)
