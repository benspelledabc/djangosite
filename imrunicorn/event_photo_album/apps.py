from django.apps import AppConfig


class EventPhotoAlbumConfig(AppConfig):
    name = 'event_photo_album'
