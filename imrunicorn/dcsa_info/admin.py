from django.contrib import admin
from .models import RequestTimeline

# Register your models here.
admin.site.register(RequestTimeline)
