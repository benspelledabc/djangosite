from django.apps import AppConfig


class DcsaInfoConfig(AppConfig):
    name = 'dcsa_info'
