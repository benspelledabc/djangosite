from django.apps import AppConfig


class DadJokesConfig(AppConfig):
    name = 'dad_jokes'
