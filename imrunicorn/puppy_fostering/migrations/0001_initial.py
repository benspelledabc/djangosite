# Generated by Django 3.0.7 on 2021-04-19 21:02

import datetime
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Momma',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(blank=True, default=None, max_length=150, null=True)),
            ],
            options={
                'ordering': ('nickname', 'id'),
            },
        ),
        migrations.CreateModel(
            name='Puppy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nickname', models.CharField(default='Unnamed Puppy', max_length=150)),
                ('birth_date', models.DateField(default=datetime.date.today)),
                ('momma', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='puppy_fostering.Momma')),
            ],
            options={
                'ordering': ('momma__nickname', 'nickname', 'id'),
            },
        ),
        migrations.CreateModel(
            name='PuppyNotes',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('note_date', models.DateField(default=datetime.date.today)),
                ('puppy_shot', models.ImageField(blank=True, null=True, upload_to='uploads/puppy_fostering/')),
                ('note', models.TextField(blank=True, null=True)),
                ('puppy', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='puppy_fostering.Puppy')),
            ],
            options={
                'ordering': ('puppy', 'note_date'),
            },
        ),
    ]
