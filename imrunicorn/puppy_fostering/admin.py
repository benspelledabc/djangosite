from django.contrib import admin
from .models import Momma, Puppy, PuppyNotes, MommaPhoto

admin.site.register(Momma)
admin.site.register(Puppy)
admin.site.register(PuppyNotes)
admin.site.register(MommaPhoto)
