# Generated by Django 3.0.7 on 2022-01-12 00:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('farminvite', '0013_remove_invitelisting_desired_time_slot'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='invitelisting',
            options={'ordering': ('Invite_Date', 'id')},
        ),
    ]
