from django import forms
from django.forms import ModelForm
from .models import InviteListing



class DateInput(forms.DateInput):
    input_type = 'date'


class InviteListingForm(forms.ModelForm):

    class Meta:
        model = InviteListing
        fields = ('Invite_Date',
                  'Invite_Display_Name', 
                  'Real_Name', 
                  'Phone_Number', 
                  'EMail')
        widgets = {
            'Invite_Date': DateInput(),
        }

