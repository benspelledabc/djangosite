from django.contrib import admin
from .models import Video, PicturesForCarousel, DAndDFifthEditionBook, FantasyGrounds, \
    RandomInsult, Secret, BuzzWordOrPhrase, SensorReadings, ArduinoUnoSketch, MotionReadings,\
    GeneralUpload

admin.site.register(GeneralUpload)
admin.site.register(Video)
admin.site.register(PicturesForCarousel)
admin.site.register(DAndDFifthEditionBook)
admin.site.register(FantasyGrounds)
admin.site.register(RandomInsult)
admin.site.register(Secret)
admin.site.register(BuzzWordOrPhrase)
admin.site.register(SensorReadings)
admin.site.register(ArduinoUnoSketch)
admin.site.register(MotionReadings)
