# Generated by Django 3.0.7 on 2021-05-06 20:36

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('content_collection', '0026_arduinounosketch_restricted'),
    ]

    operations = [
        migrations.CreateModel(
            name='MotionReadings',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('read_datetime', models.DateTimeField(auto_now_add=True)),
                ('sensor_location', models.CharField(max_length=150)),
                ('sensor_model', models.CharField(default='DHT22', max_length=150)),
                ('message', models.TextField(blank=True, null=True)),
            ],
            options={
                'verbose_name': 'Motion Reading',
                'verbose_name_plural': 'Motion Readings',
                'ordering': ('-read_datetime', '-id'),
            },
        ),
    ]
