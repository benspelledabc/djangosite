from django import forms
from .models import GeneralUpload


class GeneralUploadForm(forms.ModelForm):
    class Meta:
        model = GeneralUpload
        fields = ('path', 'file_title', 'keep_alive_until')
