from django.urls import path, include
from . import views
from .views import UpdateNewsView
from rest_framework import routers

# similar to object based url building
router = routers.DefaultRouter()
# router.register('news_api', views.NewsView)
# router.register('main_page_blurbs', views.MainPageBlurbsView)
# router.register('page_blurb_overrides', views.PageBlurbOverridesView)

# app_name = 'announcements'
urlpatterns = [
    path("", views.page_all_news, name="all_news"),
    path("", include(router.urls)),
    path("detail/", views.page_news_by_pk, name="news_by_pk"),
    path("detail/<int:news_pk>", views.page_news_by_pk, name="news_by_pk"),
    path("add/", views.page_what_is_new_add, name="what_is_new_add"),
    path("edit/", views.page_what_is_new_edit, name="what_is_new_edit"),
    path("edit/<int:news_pk>", views.page_what_is_new_edit, name="what_is_new_edit"),
    # path('edit/<int:news_pk>', UpdateNewsView.as_view(), name='what_is_new_edit'),
]
