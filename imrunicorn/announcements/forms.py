from django import forms
from .models import WhatIsNew


class AddWhatIsNewForm(forms.ModelForm):
    class Meta:
        model = WhatIsNew
        #fields = ('Blurb', 'Body', 'Published', 'Is_Sticky', 'Image_One', 'Image_Two')
        #fields = ('Blurb', 'Body', 'Published', 'Is_Sticky')
        #fields = ('Blurb', 'Body')
        fields = '__all__'


class EditWhatIsNewForm(forms.ModelForm):
    class Meta:
        model = WhatIsNew
        #fields = ('Blurb', 'Body', 'Published', 'Is_Sticky', 'Image_One', 'Image_Two')
        fields = ('Blurb', 'Body', 'Published', 'Is_Sticky')
        #fields = ('Blurb', 'Body')
        #fields = '__all__'
