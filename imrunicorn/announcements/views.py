from datetime import datetime, timedelta
from django.conf import settings
from django.db.models import F, FloatField, ExpressionWrapper
from django.http import JsonResponse, HttpResponse

# from django.http import response
from django.http.response import HttpResponseRedirect
from django.core.files.storage import FileSystemStorage
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse
import os
import json
from rest_framework.decorators import api_view, permission_classes
from django.contrib.auth.decorators import permission_required
from rest_framework.permissions import IsAuthenticated, BasePermission
from rest_framework.views import APIView
from announcements.get_news import (
    get_news,
    get_news_sticky,
    get_news_by_pk,
    get_version_json,
    get_page_blurb_override,
    get_restart_notice,
)
from .forms import AddWhatIsNewForm, EditWhatIsNewForm
from imrunicorn.functions import step_hit_count_by_page
from django.shortcuts import render
from rest_framework import viewsets
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.generic import ListView, UpdateView, DetailView, CreateView
from .forms import AddWhatIsNewForm

from .models import WhatIsNew, MainPageBlurbs, PageBlurbOverrides
from .serializer import (
    NewsSerializer,
    MainPageBlurbsSerializer,
    PageBlurbOverridesSerializer,
)

import logging

# This retrieves a Python logging instance (or creates it)
logger = logging.getLogger(__name__)


class UpdateNewsView(UpdateView):
    model = WhatIsNew
    template_name = "what_is_new_edit.html"
    fields = ["Blurb", "Body", "Published", " Is_Sticky"]


@permission_required(
    "announcements.edit_whatisnew", login_url="/login", raise_exception=True
)
def page_what_is_new_edit(request, news_pk="1"):
    # If this is a POST request then process the Form data
    logger.info("Checking for request.method == POST")
    if request.method == "POST":
        news_instance = get_object_or_404(WhatIsNew, pk=news_pk)
        form = EditWhatIsNewForm(request.POST)

        logger.info("It's a post, but is it valid?")

        if form.is_valid():
            if "action_remove" in request.POST:
                news_instance.delete()
                return HttpResponseRedirect("/announcements")

            logger.error("It's valid.. lets save it all up!")
            # news_instance.Author = form.cleaned_data['Author']
            # news_instance.Date = form.cleaned_data['Date']
            news_instance.Blurb = form.cleaned_data["Blurb"]
            news_instance.Body = form.cleaned_data["Body"]

            is_private = request.POST.get("is_private", False)

            # if request.FILES['Image_One']:
            #     Image_One = request.FILES['Image_One']
            #     fs = FileSystemStorage()
            #     filename = fs.save(Image_One.name, Image_One)

            # if request.FILES['Image_Two']:
            #     Image_Two = request.FILES['Image_Two']
            #     fs = FileSystemStorage()
            #     filename = fs.save(Image_Two.name, Image_Two)

            if request.POST.get("published" + str(news_instance.id)) == "clicked":
                # logger.info("It's a post, but is it valid?")
                news_instance.Published = True
            else:
                # logger.info("It's a post, but is it valid?")
                news_instance.Published = False

            if request.POST.get("sticky" + str(news_instance.id)) == "clicked":
                # logger.info("It's a post, but is it valid?")
                news_instance.Is_Sticky = True
            else:
                # logger.info("It's a post, but is it valid?")
                news_instance.Is_Sticky = False

            news_instance.save()

            logger.info("Sending somewhere else...")
            # redirect to a new URL:
            return HttpResponseRedirect("/announcements/edit/%d" % news_pk)

    step_hit_count_by_page(request.path)
    blurb_page = "/news/detail/" + str(news_pk)
    context = {
        # "restart": get_restart_notice,
        "copy_year": datetime.now().year,
        "news_pk": get_news_by_pk(news_pk),
        "release": get_version_json(),
        "title": "News: Full Story",
        # "blurb": "This will show the full story for the article you're reading.",
        # "blurb": get_page_blurb_override('news/'),
        "blurb": get_page_blurb_override(blurb_page),
    }

    return render(request, "announcements/what_is_new-edit.html", context)


@permission_required(
    "announcements.add_whatisnew", login_url="/login", raise_exception=True
)
def page_what_is_new_add(request):
    if request.method == "POST":
        form = AddWhatIsNewForm(request.POST, request.FILES)
        if form.is_valid():
            form.save()
            # return redirect('announcements:all_news')
            return HttpResponseRedirect("/announcements/")
    else:
        form = AddWhatIsNewForm()
        # form.fields['keep_alive_until'].initial = "2013-04-28 21:54"
        # form.fields['keep_alive_until'].initial = datetime.now() + timedelta(days=3)

    step_hit_count_by_page(request.path)
    dataset = get_news(request)

    context = {
        "form": form,
        "copy_year": datetime.now().year,
        "release": get_version_json(),
        "title": "Announcements: What Is New",
        "secrets": dataset,
        "blurb": get_page_blurb_override("announcements/"),
    }
    return render(request, "announcements/what_is_new-add.html", context)


# Create your views here.
def page_all_news(request):
    step_hit_count_by_page(request.path)
    # logger.info("This is not getting logged...")
    context = {
        # "restart": get_restart_notice,
        "copy_year": datetime.now().year,
        "all_news": get_news(request),
        "release": get_version_json(),
        "title": "All the news",
        "blurb": get_page_blurb_override("announcements/"),
    }
    return render(request, "announcements/all_news.html", context)


def json_all_news_json(request):
    step_hit_count_by_page(request.path)
    context = {
        # "restart": get_restart_notice,
        "name": "peter griffin",
        "channel": "fox",
    }
    return JsonResponse(context)


def page_news_by_pk(request, news_pk="1"):
    step_hit_count_by_page(request.path)
    blurb_page = "/news/detail/" + str(news_pk)
    context = {
        # "restart": get_restart_notice,
        "copy_year": datetime.now().year,
        "news_pk": get_news_by_pk(news_pk),
        "release": get_version_json(),
        "title": "News: Full Story",
        # "blurb": "This will show the full story for the article you're reading.",
        # "blurb": get_page_blurb_override('news/'),
        "blurb": get_page_blurb_override(blurb_page),
    }
    return render(request, "announcements/news_by_pk.html", context)


# @api_view(['GET'])
# @permission_classes([IsAuthenticated])
class NewsView(viewsets.ModelViewSet):
    queryset = WhatIsNew.objects.all()
    serializer_class = NewsSerializer


class MainPageBlurbsView(viewsets.ModelViewSet):
    queryset = MainPageBlurbs.objects.all()
    serializer_class = MainPageBlurbsSerializer


class PageBlurbOverridesView(viewsets.ModelViewSet):
    queryset = PageBlurbOverrides.objects.all()
    serializer_class = PageBlurbOverridesSerializer
