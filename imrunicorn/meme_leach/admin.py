from django.contrib import admin
from .models import LeachedMeme, LeachedMemePreview

# Register your models here.
admin.site.register(LeachedMeme)
admin.site.register(LeachedMemePreview)
