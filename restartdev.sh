#!/bin/bash

docker-compose -f docker-compose-build.yml down
docker-compose -f docker-compose-build.yml up -d --build
docker exec -it djangosite-unicorn_web-1 python imrunicorn/manage.py collectstatic --no-input
docker exec -it djangosite-unicorn_web-1 python imrunicorn/manage.py makemigrations
docker exec -it djangosite-unicorn_web-1 python imrunicorn/manage.py migrate
docker-compose -f docker-compose-build.yml logs -f
