# .bashrc

# User specific aliases and functions

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

alias dcr="docker-compose down && docker-compose up -d"
alias dcub='docker-compose -f docker-compose-build.yml up --build'

#source ~/.bash/git-prompt.sh # Show git branch name at command prompt
#export GIT_PS1_SHOWCOLORHINTS=true # Option for git-prompt.sh to show branch name in color

# Terminal Prompt:
# Include git branch, use PROMPT_COMMAND (not PS1) to get color output (see git-prompt.sh for more)
#export PROMPT_COMMAND='__git_ps1 "\w" "\n\\\$ "' # Git branch (relies on git-prompt.sh)

COLOR_RED="\033[0;31m"
COLOR_BOLD_RED="\033[1;31m"
COLOR_YELLOW="\033[0;33m"
COLOR_BOLD_YELLOW="\033[1;33m"
COLOR_GREEN="\033[0;32m"
COLOR_BOLD_GREEN="\033[1;32m"
COLOR_OCHRE="\033[38;5;95m"
COLOR_BLUE="\033[0;34m"
COLOR_WHITE="\033[0;37m"
COLOR_BOLD_WHITE="\033[1;37m"
COLOR_RESET="\033[0m"

git_branch() {
    git branch --no-color 2>/dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/ '
}

git_status() {
    local status="$(git status --porcelain 2>/dev/null)"
    local output=''
    [[ -n $(egrep '^[MADRC]' <<<"$status") ]] && output="$output+"
    [[ -n $(egrep '^.[MD]' <<<"$status") ]] && output="$output!"
    [[ -n $(egrep '^\?\?' <<<"$status") ]] && output="$output?"
    [[ -n $(git stash list) ]] && output="${output}S"
    [[ -n $(git log --branches --not --remotes) ]] && output="${output}P"
    [[ -n $output ]] && output="|$output"
    echo $output
}

git_color() {
   local staged=$([[ $1 =~ \+ ]] && echo yes) 
   local dirty=$([[ $1 =~ [!\?] ]] && echo yes)
   if [[ -n $staged ]] && [[ -n $dirty ]]; then
      echo -e $COLOR_BOLD_YELLOW
   elif [[ -n $staged ]]; then
      echo -e $COLOR_BOLD_GREEN
   elif [[ -n $dirty ]]; then
      echo -e $COLOR_BOLD_RED
   else
      echo -e $COLOR_BOLD_WHITE
   fi
}

git_prompt() {
   local branch=$(git_branch)
   if [[ -n $branch ]]; then
      local state=$(git_status)
      local color=$(git_color  $state)
      echo -e "\x01$color\x02[$branch$state]\x01\033[00m\x02"
   fi
}

export PS1='$debian_chroot\[\033[0;32m\]\u@\h\[\033[00m\]:\[\033[38;5;215m\]\W$(git_prompt)\[\033[00m\]\$ '





