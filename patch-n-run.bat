@echo off
python imrunicorn\manage.py makemigrations
python imrunicorn\manage.py migrate
python imrunicorn\manage.py collectstatic --noinput
python imrunicorn\manage.py runserver